﻿using MedicDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMedicDB
{
    public class Program
    {
        static void Main(string[] args)
        {
            DAOMedecin daomed = new DAOMedecin();
            Medecin m = daomed.read(1);
            Console.WriteLine($"id:{m.IdMedecin} | nom : {m.Nom} | prenom : {m.Prenom} | adresse : {m.Adresse} | numINAMI : {m.NumINAMI}| specialisation : {m.NomSpecialisation}");

            Console.WriteLine();


            
            
            Personne pers1 = new Personne("Darc", "Jeane", "Aven. Troisieme 15", "00314578");
            Console.WriteLine(pers1);

            Console.WriteLine();

            DAOPatient daopat = new DAOPatient();

            Patient patient1 = new Patient("Bruel", "Jean", "Rue Gogo 12", "032 512487", "1976/02/02", "homme", "225 226", "2001/10/10", "J.Kroll", "Mutuelle Socialist", "D.Dupont");
            patient1 = daopat.create(patient1);


            DAOPatient daopat1 = new DAOPatient();
            Patient p = daopat1.read(1);
            Console.WriteLine($"id:{p.IdPatient} | nom : {p.Nom} | prenom : {p.Prenom} | adresse : {p.Adresse} | redistreNational {p.RegistreNational}| sexe : {p.Sexe} | medecinReferant : {p.MedecinReferant} | mutuelle : {p.Mutuelle} | tierPayant : {p.TierPayant}");

            Console.WriteLine();

            DAORdv daordv = new DAORdv();
            //test1
            DateTime date = new DateTime(2017, 12, 16, 9, 30, 0);
            Rdv r1 = new Rdv(date, m, p);
            r1= daordv.create(r1);
            if (r1 != null)
            {
                Console.WriteLine(r1.IdRdv);
            }

            //Test2
            DateTime date2 = new DateTime(2017, 12, 12, 13, 0, 0);
            Rdv r2 = new Rdv(date, m, p);
            r2 = daordv.create(r2);
            if (r2 != null)
            {
                Console.WriteLine(r2.IdRdv); //consoleje matom tik rdv numeri
            }

            Console.ReadLine();
            
        }
    }
}
