﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Specialisation
    {
        public int IdSpecialisation { get; set; }
        public string Nom { get; set; }

     
        public Specialisation(int idSpecialisation, string nom)
        {
            IdSpecialisation = idSpecialisation;
            Nom = nom;
        }
        public Specialisation()
        {

        }
    }
}
