﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Personalisation
    {
        public enum choixType { Color = 1, Font = 2 }
        private int _idPersonalisation;
        private int _idUser;
        private string _libelle;
        private string _valeur;
        private choixType _type;
        public int IdPersonalisation
        {
            get { return _idPersonalisation; }
            set { _idPersonalisation = value; }
        }

        public int IdUser
        {
            get
            {
                return _idUser;
            }

            set
            {
                _idUser = value;
            }
        }


        public string Libelle
        {
            get
            {
                return _libelle;
            }

            set
            {
                _libelle = value;
            }
        }

        public string Valeur
        {
            get
            {
                return _valeur;
            }

            set
            {
                _valeur = value;
            }
        }

        public choixType Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }
    }
}
