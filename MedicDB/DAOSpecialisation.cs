﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAOSpecialisation : DAO, IDAO<Specialisation>
    {
        public Specialisation create(Specialisation obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Specialisation read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Specialisation> readAll()
        {
            List<Specialisation> listSpec = new List<Specialisation>();

            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM specialisation";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Specialisation specialisation = null;
            foreach (DataRow row in dt.Rows)
            {
                   specialisation = new Specialisation(
                   (int)row["idSpecialisation"],
                   row["nom"].ToString());

                listSpec.Add(specialisation);
            }
            return listSpec;
        }

        public Specialisation update(Specialisation obj)
        {
            throw new NotImplementedException();
        }
    }
}
