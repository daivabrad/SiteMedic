﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAOMedecin: DAO, IDAO<Medecin>
    {
        public Medecin create(Medecin obj)
        {
            Utilisateur utilisateur = obj.Utilisateur1;
            
            SqlCommand commandCreatePerson = connection.CreateCommand();
            string queryCreatePerson = "INSERT INTO personne (nom, prenom, adresse, num_telephone) " +
               "OUTPUT inserted.idPersonne " +
               "VALUES (@nom, @prenom, @adresse, @num_telephone)";

            commandCreatePerson.CommandText = queryCreatePerson;

            commandCreatePerson.Parameters.AddWithValue("@nom", obj.Nom);
            commandCreatePerson.Parameters.AddWithValue("@prenom", obj.Prenom);
            commandCreatePerson.Parameters.AddWithValue("@adresse", obj.Adresse);
            commandCreatePerson.Parameters.AddWithValue("@num_telephone", obj.NumTelephone);

            connection.Open();
            int idPersonne = (int)commandCreatePerson.ExecuteScalar();
            connection.Close();

            SqlCommand commandUt = connection.CreateCommand();

            string queryUt = "INSERT INTO utilisateur (login, password) " +
            "OUTPUT inserted.idUtilisateur " +
            "VALUES (@login, @password)";

            commandUt.CommandText = queryUt;

            commandUt.Parameters.AddWithValue("@login", obj.Utilisateur1.Login);
            commandUt.Parameters.AddWithValue("@password", obj.Utilisateur1.Password);

            connection.Open();
            int idUtilisateur = (int)commandUt.ExecuteScalar();
            connection.Close();

            //SqlCommand commandRecuperateSpec = connection.CreateCommand();
            //string queryCreateSpec = "SELECT idSpecialisation FROM specialisation " +
            //"WHERE nom=@nom";

            //commandRecuperateSpec.CommandText = queryCreateSpec;
            //commandRecuperateSpec.Parameters.AddWithValue("@nom", obj.NomSpecialisation);
            int idSpecialisation = obj.IdSpecialisation;
            connection.Close();
            

            SqlCommand commandCreateMedecin = connection.CreateCommand();
            string queryCreateMedecin = "INSERT INTO medecin (numINAMI, fk_idPersonne, " +
            "fk_idUtilisateur, fk_idSpecialisation) " +
            "OUTPUT inserted.idMedecin " +
            "VALUES (@numINAMI, @idPersonne, @idUtilisateur, @idSpecialisation)";

            commandCreateMedecin.CommandText = queryCreateMedecin;

            commandCreateMedecin.Parameters.AddWithValue("@numINAMI", obj.NumINAMI);
            commandCreateMedecin.Parameters.AddWithValue("@idPersonne", idPersonne);
            commandCreateMedecin.Parameters.AddWithValue("@idUtilisateur", idUtilisateur);
            commandCreateMedecin.Parameters.AddWithValue("@idSpecialisation", idSpecialisation);

            connection.Open();
            int id = (int)commandCreateMedecin.ExecuteScalar();
            connection.Close();

            return obj;
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Medecin read(string nom)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT p.nom as 'nomMedecin', prenom, adresse, num_telephone, s.nom as 'nomSpecialisation', idMedecin, numINAMI " +
                "FROM medecin m INNER JOIN specialisation s ON m.fk_idSpecialisation = s.idSpecialisation " +
                "INNER JOIN personne p ON " +
                "m.fk_idPersonne = p.idPersonne " +
                "WHERE p.nom LIKE CONCAT(%+@nom+%)";

            command.CommandText = query;
            command.Parameters.AddWithValue("@nom", nom);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Medecin medecin = null;
            foreach (DataRow row in dt.Rows)
            {
                medecin = mapperMedecin(row);
            }

            return medecin;
        }

        public Medecin read(int id)
        {
            
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT p.nom as 'nomMedecin', prenom, adresse, num_telephone, s.nom as 'nomSpecialisation', idMedecin, numINAMI " +
                "FROM medecin m INNER JOIN specialisation s ON m.fk_idSpecialisation = s.idSpecialisation " +
                "INNER JOIN personne p ON " +
                "m.fk_idPersonne = p.idPersonne " +
                "WHERE idMedecin= " + id;

                command.CommandText=query;

            SqlDataAdapter da = new SqlDataAdapter();
            //Il contient un objet SelectComment
            da.SelectCommand = command;
            //on peut avoir plusieurs DataTable dans DataSet: une table c'est UNE requête
            //Dans les DataTable on a des DataRow et des DataColumn
            //comme ici on n'a qu'une seule query, on appelle directement DataTable et pas DataSet
            DataTable dt = new DataTable();
            //on rempli notre DataTable avec la requête
            da.Fill(dt);
            Medecin medecin = null;
            foreach (DataRow row in dt.Rows)
            {
                //kad nereiketu kartoti to paties 
                //(kas dabar yra mapperMedecin) metode, parasom tiesiog sita metoda
                medecin = mapperMedecin(row);
            }
            
            return medecin;
        }

        public Medecin read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Medecin> readAll()
        {
            List<Medecin> medecins = new List<Medecin>();

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT p.nom as 'nomMedecin', prenom, adresse, num_telephone, " + 
            "s.nom as 'nomSpecialisation', idMedecin, numINAMI " +
            "FROM medecin m INNER JOIN specialisation s ON m.fk_idSpecialisation = s.idSpecialisation " +
            "INNER JOIN personne p ON " +
            "m.fk_idPersonne = p.idPersonne";
            
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter(command);

            DataTable dt = new DataTable();
            da.Fill(dt);
         
            foreach (DataRow row in dt.Rows)
            {
                Medecin medecin = mapperMedecin(row);
                medecins.Add(medecin);
            }                     
            return medecins;
        }
        public Medecin mapperMedecin(DataRow row)
        {
            //transformer en objet, bus naudojama bet kokioje pieskoje read() ar readAll()
                          Medecin medecin = new Medecin(
                          (int)row["idMedecin"],
                          row["nomMedecin"].ToString(),
                          row["prenom"].ToString(),
                          row["adresse"].ToString(),
                          row["num_telephone"].ToString(),
                          row["nomSpecialisation"].ToString(),
                          row["numInami"].ToString());

                          return medecin;
        }
        
        public Medecin update(Medecin obj)
        {
            throw new NotImplementedException();
        }
    }
}
