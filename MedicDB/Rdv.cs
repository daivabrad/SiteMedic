﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Rdv
    {
        private int idRdv;
        private DateTime date;
        private Medecin medecin;
        private Patient patient;

        
        public Medecin Medecin { get => medecin; set => medecin = value; }
        public Patient Patient { get => patient; set => patient = value; }
        public DateTime Date { get => date; set => date = value; }
        public int IdRdv { get => idRdv; set => idRdv = value; }

        public Rdv(int idRdv, DateTime date, Medecin medecin, Patient patient)
        {
            IdRdv = idRdv;
            Date = date;
            Medecin = medecin;
            Patient = patient;
        }

        public Rdv(DateTime date, Medecin medecin, Patient patient)
        {
            Date = date;
            Medecin = medecin;
            Patient = patient;
        }

        public Rdv()
        {

        }
    }


}
