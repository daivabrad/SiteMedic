﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MedicDB.Personalisation;

namespace MedicDB
{
    public class DAOPersonalisation : DAO, IDAO<Personalisation>
    {
        public Personalisation create(Personalisation obj)
        {
            SqlCommand command = connection.CreateCommand();
            string query = string.Format(@"INSERT INTO UserPerso (idUtilisateur, idPersonalisation, value) VALUES (@idUser, @idPerso, @value )");
            command.CommandText = query;
            command.Parameters.AddWithValue("@idUser", obj.IdUser);
            command.Parameters.AddWithValue("@idPerso", obj.IdPersonalisation);
            command.Parameters.AddWithValue("@value", obj.Valeur);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            return obj;
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Personalisation read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Personalisation> readAll(int id)
        {
            SqlCommand Cmd = connection.CreateCommand();
            string query = @"SELECT U.idPersonalisation, U.idUtilisateur, U.Valeur, P.Libelle, T.Libelle as LibPerso
                            FROM            UserPerso AS U INNER JOIN
                                                     Personalisation AS P ON U.idPersonalisation = P.idPersonalisation INNER JOIN
                                                     TypePersonalisation T ON P.idTypePersonalisation = T.idTypePersonalisation
                            where idUtilisateur=@idUser";
            Cmd.CommandText = query;

            List<Personalisation> LP = new List<Personalisation>();
            Cmd.Parameters.AddWithValue("@idUser", id);
            Cmd.Connection.Open();
            SqlDataReader odr = Cmd.ExecuteReader();
            while (odr.Read())
            {
                Personalisation perso = new Personalisation()
                {
                    IdPersonalisation = (int)odr["idPersonalisation"],
                    IdUser = (int)odr["idUtilisateur"],
                    Valeur = odr["Valeur"].ToString(),
                    Libelle = odr["Libelle"].ToString(),
                    Type = (choixType)Enum.Parse(typeof(choixType), odr["LibPerso"].ToString())
                };
                LP.Add(perso);
            }
            odr.Close();
            Cmd.Connection.Close();
            return LP;
        }

        public List<Personalisation> readAll()
        {
            throw new NotImplementedException();
        }

        public Personalisation update(Personalisation obj)
        {
            SqlCommand command = connection.CreateCommand();
            string query = string.Format(@"Update UserPerso  SET [Valeur] = @value
                                            WHERE [idUtilisateur] = @idUser and [idPersonalisation] = @idPerso");
            command.CommandText = query;
            command.Parameters.AddWithValue("@idUser", obj.IdUser);
            command.Parameters.AddWithValue("@idPerso", obj.IdPersonalisation);
            command.Parameters.AddWithValue("@value", obj.Valeur);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            return obj;

        }
    }
}
