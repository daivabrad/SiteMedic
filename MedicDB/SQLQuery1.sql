﻿CREATE TABLE personne(
idPersonne int PRIMARY KEY IDENTITY(1,1),
nom varchar(100) NOT NULL,
prenom varchar(100) NOT NULL,
adresse varchar(250) NOT NULL,
num_telephone varchar(20)
)

CREATE TABLE utilisateur (
idUtilisateur int PRIMARY KEY IDENTITY(1,1),
[login] varchar(50) NOT NULL,
[password] varchar(50) NOT NULL
)

CREATE TABLE secretaire (
idSecretaire int PRIMARY KEY IDENTITY(1,1),
[service] varchar(250) NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne),
fk_idUtilisateur int NOT NULL,

CONSTRAINT fk_secretaire_utilisateur FOREIGN KEY (fk_idUtilisateur) REFERENCES utilisateur (idUtilisateur)
)

CREATE TABLE specialisation (
idSpecialisation int PRIMARY KEY IDENTITY(1,1),
nom varchar(100) NOT NULL
)



CREATE TABLE medecin (
idMedecin int PRIMARY KEY IDENTITY(1,1),
numINAMI varchar(50) NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne),
fk_idUtilisateur int NOT NULL,
fk_idSpecialisation int NOT NULL REFERENCES specialisation (idSpecialisation),
CONSTRAINT fk_medecin_utilisateur FOREIGN KEY (fk_idUtilisateur) REFERENCES utilisateur (idUtilisateur)
)

CREATE TABLE mutuelle(
idMutuelle int PRIMARY KEY IDENTITY(1,1),
nom varchar (150) NOT NULL
)

CREATE TABLE patient(
idPatient int PRIMARY KEY IDENTITY(1,1),
dateNaissance DATE NOT NULL,
num_registre_national varchar (25) UNIQUE NOT NULL,
sexe varchar (50) NOT NULL CHECK (sexe IN ('female', 'male', 'other')),

--arba taip:
----sexe2 varchar (50) NOT NULL,
----CONSTRAINT ck_sexe CHECK (sexe2),

medecinReferant varchar (150),
tierPayant varchar (100),
dateInscription date NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne),
fk_idMutuelle int NOT NULL REFERENCES mutuelle (idMutuelle)
)

CREATE TABLE dossierMedical(
idDossierMed int PRIMARY KEY IDENTITY(1,1),
fk_idPatient int NOT NULL REFERENCES patient (idPatient)
)

CREATE TABLE consultation(
idConsultation int PRIMARY KEY IDENTITY(1,1),
dateConsult date NOT NULL,
objet varchar (200) NOT NULL,
observations varchar(500),
fr_idMedecin int NOT NULL REFERENCES medecin (idMedecin),
--fr_idPrescriptionMedicament int NOT NULL REFERENCES prescriptionMedicament (idPrescriptionMedicament),
--fr_idPrescriptionExam int NOT NULL REFERENCES prescriptionExament (idPrescriptionExam),
fr_idDossierMed int NOT NULL REFERENCES dossierMedical (idDossierMed)
)


CREATE TABLE rdv(
idRdv int PRIMARY KEY IDENTITY(1,1),
[date] datetime NOT NULL,
fr_idMedecin int NOT NULL REFERENCES medecin (idMedecin),
fr_idPatient int NOT NULL REFERENCES patient (idPatient)
)

CREATE TABLE typeMedicament(
idTypeMedicament int PRIMARY KEY IDENTITY(1,1),
nom varchar (150),
substanceActive varchar (150),
estGenerique bit,
prix money NOT NULL
)

--cia as istryniau fr_idDossierMedical, reikia surinkti drop table ir nauja - su pakeitimais - table ir paspausti zalia trikampi
DROP TABLE prescriptionMedicament

CREATE TABLE prescriptionMedicament(
idPrescriptionMedicament int PRIMARY KEY IDENTITY(1,1),
nbrJours int NOT NULL,
posologie varchar (200) NOT NULL,
fr_idTypeMedicament int NOT NULL REFERENCES typeMedicament (idTypeMedicament),
fr_idConsultation int NOT NULL REFERENCES consultation (idConsultation)
)


CREATE TABLE typeExament(
idTypeExam int PRIMARY KEY IDENTITY(1,1),
nom varchar (150) NOT NULL
)

--cia as istryniau fr_idDossierMedical
DROP TABLE prescriptionExament

CREATE TABLE prescriptionExament(
idPrescriptionExam int PRIMARY KEY IDENTITY(1,1),
remarques varchar (MAX),
localisation varchar (MAX) NOT NULL,
fr_idTypeExam int NOT NULL REFERENCES typeExament (idTypeExam),
fr_idConsultation int NOT NULL REFERENCES consultation (idConsultation)
)


CREATE TABLE pathologie (
idPathologie int PRIMARY KEY IDENTITY(1,1),
nom varchar (100) NOT NULL
)


--association table between consultation and maladie
CREATE TABLE consultation_pathologie(
idConsultationPathologie int PRIMARY KEY IDENTITY(1,1),
fr_idPathologie int NOT NULL REFERENCES pathologie (idPathologie),
fr_idConsultation int NOT NULL REFERENCES consultation (idConsultation),
dateDebut date NOT NULL,
dateFin date,
contrIndication varchar (400)
)



INSERT INTO personne (nom, prenom, adresse, num_telephone)
VALUES 
	('Tamin', 'Eric', 'Chemin de la sucrerie', '0492 45 89 74'),
	('Marl', 'Steven', 'Chemin des vignes', '0497 84 25 22'),
	('Pirmez', 'Jean', 'Rue de fer', '0497 81 58 44'),
	('Alba', 'Marc', 'Avenue de l''église', '0496 41 12 56'),
	('Petit', 'Robert', 'Boulevard des échasseurs', '0492 25 85 96'),
	('Strueman', 'Laura', 'Chemin de la sciences', '0495 73 15 97'),
	('Pitt', 'Noémie', 'Rue du parc', '0495 89 89 81'),
	('Mc Karter', 'Maxime', 'Rue des nobles', '0495 82 56 57'),
	('Nuila', 'Laetitia', 'Rue du commerce', '0492 21 98 28'),
	('Ghalzi', 'Julie', 'Avenue des grands maréchals', '0499 56 89 71'),
	('Falzo', 'Giuseppe', 'Avenue Léopold', '0497 84 35 27'),
	('Dupont', 'Charles', 'Avenue royale', '0492 63 23 72'),
	('Grali', 'Marie', 'Rue de Limal', '0498 54 25 51'),
	('Liron', 'Nicolas', 'Avenue de l''Europe', '0497 04 24 82'),
	('Jano', 'Laurence', 'Rue du Monastere', '0494 05 43 90'),
	('Gratin', 'Joëlle', 'Rue du Réservoir', '0492 09 63 21')

INSERT INTO utilisateur (login, password)
VALUES
	('med1', 'pmed1'),
	('med2', 'pmed2'),
	('med3', 'pmed3'),
	('med4', 'pmed4'),
	('med5', 'pmed5'),
	('med6', 'pmed6')

INSERT INTO specialisation (nom)
VALUES
	('anesthésiologie'),
	('allergologie'),
	('andrologie'),
	('cardiologie'),
	('chirurgie cardiaque'),
	('chirurgie plastique'),
	('chirurgie générale'),
	('chirurgie vasculaire'),
	('dermatologie'),
	('endocrinologie'),
	('gériatrie')

INSERT INTO medecin (numINAMI, fk_idPersonne, fk_idUtilisateur, fk_idSpecialisation)
VALUES
	('4585 8565', 1, 1, 5),
	('8640 2501', 2,2,3),
	('9855 4209', 3,3,2),
	('4735 6528', 4,4,6),
	('9456 7852', 5,5,7),
	('2014 6852', 6,6,1)

INSERT INTO patient (DDN, sexe, registreNational, medecinReferant, mutuelle, tierPayant, dateInscription, fk_idPersonne)
VALUES
	('1985-10-14', 'homme', '851014-654-82', 'Eric Tamin', null, null, '2000-07-05', 8),
	('1999-11-25', 'femme', '991125-468-52', 'Luc Marbre', null, null, '2005-11-08', 7),
	('2001-01-07', 'homme', '010107-945-25', 'Laura Strueman', null, null, '2012-04-12', 11),
	('1960-03-17', 'femme', '600317-589-91', 'Michel Nirk', null, null, '2002-03-08', 9),
	('1976-10-18', 'homme', '761018-853-46', 'Robert Petit', null, null, '1998-02-05', 12),
	('1985-08-30', 'femme', '850830-482-01', 'Laura Mulaire', null, null, '1997-08-29', 10)

INSERT INTO rdv (date, fk_idMedecin, fk_idPatient)
VALUES
	('2017-12-04 08:0:0', 1,1),
	('2017-12-11 08:0:0', 1,1),
	('2017-12-21 08:0:0', 1,1),
	('2017-12-22 08:0:0', 1,1),
	('2017-12-29 13:0:0', 1,1),
	('2017-12-04 13:0:0', 1,2),
	('2017-12-15 08:0:0', 1,2),
	('2017-12-19 13:0:0', 1,2),
	('2017-12-06 08:0:0', 2,2),
	('2017-12-11 08:0:0', 2,2),
	('2017-12-21 08:0:0', 2,2),
	('2017-12-08 08:0:0', 6,3),
	('2017-12-21 08:0:0', 6,3),
	('2017-12-22 08:0:0', 6,3),
	('2017-12-25 08:0:0', 6,3),
	('2017-12-14 13:0:0', 3,4),
	('2017-12-15 13:0:0', 6,4),
	('2017-12-18 13:0:0', 1,4),
	('2017-12-19 08:0:0', 1,4),
	('2017-12-06 13:0:0', 5,5),
	('2017-12-15 08:0:0', 5,5),
	('2017-12-22 13:0:0', 5,5),
	('2017-12-13 13:0:0', 5,5),
	('2017-12-14 13:0:0', 4,6),
	('2017-12-06 13:0:0', 2,6),
	('2017-12-21 13:0:0', 1,6),
	('2017-12-22 13:0:0', 1,6)
	


