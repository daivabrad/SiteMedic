﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Secretaire : Personne
    {
        private int idSecretaire;
        private string service;
        private Utilisateur Utilisateur;

       

        public int IdSecretaire { get => idSecretaire; set => idSecretaire = value; }
        public string Service { get => service; set => service = value; }
        public Utilisateur Utilisateur1 { get => Utilisateur; set => Utilisateur = value; }

        public Secretaire(string nom, string prenom, string adresse, string numTelephone, string service, Utilisateur utilisateur) 
            : base(nom, prenom, adresse, numTelephone)
        {
            Service = service;
            Utilisateur1 = utilisateur;
        }

        public Secretaire(int idSecretaire, string nom, string prenom, string adresse, string numTelephone, string service, Utilisateur utilisateur)
        : this (nom, prenom, adresse, numTelephone, service, utilisateur)
        {
            IdSecretaire = idSecretaire;
        }
    }

}
