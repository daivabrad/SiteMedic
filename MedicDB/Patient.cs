﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Patient : Personne
    {
        //string v;

        public int IdPatient { get; set; }
        public DateTime DDN { get; set; }
        public string Sexe { get; set; }
        public string RegistreNational { get; set; }
        public string MedecinReferant { get; set; }
        public string Mutuelle { get; set; }
        public string TierPayant { get; set; }
        public DateTime DateInscription { get; set; }


        DateTime dt = new DateTime(12 / 12 / 97);

        public Patient(int idPatient, string nom, string prenom, string adresse, string numTelephone, DateTime ddn, string sexe, string registreNational, string medecinReferant, string mutuelle, string tierPayant, DateTime dateInscription)
        {
            IdPatient = idPatient;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumTelephone = numTelephone;
            DDN = ddn;
            Sexe = sexe;
            RegistreNational = registreNational;
            MedecinReferant = medecinReferant;
            Mutuelle = mutuelle;
            TierPayant = tierPayant;
            DateInscription = dateInscription;
        }
        public Patient()
        {

        }
        #region 
        //Constructeur du Patient avec les arguments de Personne (sans id patient)
        public Patient(string nom, string prenom, string adresse, string numTelephone, DateTime ddn, string sexe, string registreNational, DateTime dateInscription) : base(nom, prenom, adresse, numTelephone)
        {
            DDN = ddn;
            Sexe = sexe;
            RegistreNational = registreNational;
            DateInscription = dateInscription;
        }
        #endregion

        #region 
        //Constructeur du Patient avec les arguments de Personne (avec id patient)
        public Patient(int idPatient, string nom, string prenom, string adresse, string numTelephone, DateTime ddn, string sexe, string registreNational, DateTime dateInscription) : this(nom, prenom, adresse, numTelephone, ddn, sexe, registreNational, dateInscription)
        {
            IdPatient = idPatient;
        }
        #endregion

        #region 
        //Constructeur du Patient avec les arguments de Personne et ceux qui sont nullable dans la DB
        public Patient(string nom, string prenom, string adresse, string numTelephone, DateTime ddn, string sexe, string registreNational, DateTime dateInscription, string medecinReferant, string mutuelle, string tierPayant)
            : this(nom, prenom, adresse, numTelephone, ddn, sexe, registreNational, dateInscription)
        {
            MedecinReferant = medecinReferant;
            Mutuelle = mutuelle;
            TierPayant = tierPayant;
        }

        public Patient(string nom, string prenom, string adresse, string numTelephone, DateTime ddn, DateTime dateInscription) : base(nom, prenom, adresse, numTelephone)
        {
            this.DateInscription = dateInscription;
        }
        #endregion
    }
}
