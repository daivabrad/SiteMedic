﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAORdv : DAO, IDAO<Rdv>
    {
        public Rdv create(Rdv obj)
        {
            Medecin med = obj.Medecin;
            Patient pat = obj.Patient;
            DateTime date = obj.Date;

            List<Rdv> listRdvMedecin = readAllByPerson(med);
            //rm - rdv en question, tikrinam, ar tas laikas jau uzimtas, 
            //ar ne. Where.rm=> - yra kaip foreach; rm.Date - laikas, kurio nori klientas, 
            //Date - Rdv obj.
            Rdv r = listRdvMedecin.Where(rm => rm.Date == date)
                //atsiuncia viena arba nieko
                .SingleOrDefault();
            //jei tas laikas jau uzimtas, return nieko
            if (r != null)
            {
                return null;
            }

            List<Rdv> listRdvPatient = readAllByPerson(pat);
            Rdv r1 = listRdvPatient.Where(rm => rm.Date == date)
                .SingleOrDefault();
            if (r1 != null)
            {
                return null;
            }

            SqlCommand command = connection.CreateCommand();

            //NonQuery informuoja, kiek liniju ir kuriu, jis neduoda saraso ar duomenu
            //command.ExecuteNonQuery(); pour update ir delete
            //command.ExecuteScalar();  jis iesko vieno duomens ir ji parsiuncia
            //executeReader - kai ieskom daug duomenu

            string query = "INSERT INTO rdv (date, fk_idMedecin, fk_idPatient) " +
                "OUTPUT inserted.idRdv " +
                "VALUES (@date, @idMedecin, @idPatient)";

            command.CommandText = query;
            //abra sitas :
            SqlParameter paramDate = new SqlParameter("@date", obj.Date);
            //paramDate yra values
            command.Parameters.Add(paramDate);

            //arba sitas metodas:
            command.Parameters.AddWithValue("@idMedecin", obj.Medecin.IdMedecin);
            command.Parameters.AddWithValue("@idPatient", pat.IdPatient);

            // connection.Open() galima parasyti ir pries SqlCommand command 
            connection.Open();
            int id = (int)command.ExecuteScalar();
            connection.Close();

            obj.IdRdv = id;
            return obj;

        }

        public void delete(int id)
        {
            SqlCommand commandDeleteRdv = connection.CreateCommand();
            string query = "DELETE FROM rdv WHERE idRdv= " + id;

            commandDeleteRdv.CommandText = query;

            connection.Open();
            commandDeleteRdv.ExecuteNonQuery();
            connection.Close();

        }

        public Rdv read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM rdv WHERE idRdv = " + id;
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
           
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOMedecin daoMed = new DAOMedecin();
            DAOPatient daoPat = new DAOPatient();

            Rdv rdv = null;
            foreach (DataRow row in dt.Rows)
            {
                Medecin med;
                Patient pat;
                med = daoMed.read((int)row["fk_idMedecin"]);
                pat = daoPat.read((int)row["fk_idPatient"]);

                rdv=new Rdv(
                   (int)row["idRdv"],
                   (DateTime)row["date"], med, pat);
            }
            return rdv;
        }

        public Rdv read(object id)
        {
            throw new NotImplementedException();
        }
        public Rdv readAll(object id)
        {
            throw new NotImplementedException();
        }


        public List<Rdv> readAll()
        {
            List<Rdv> listRdv = new List<Rdv>();

            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM rdv";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOMedecin daoMed = new DAOMedecin();
            DAOPatient daoPat = new DAOPatient();

            foreach (DataRow row in dt.Rows)
            {
                Medecin med;
                Patient pat;
                med = daoMed.read((int)row["fk_idMedecin"]);
                pat = daoPat.read((int)row["fk_idPatient"]);

                Rdv rdv = new Rdv(
                   (int)row["idRdv"],
                   (DateTime)row["date"], med, pat);

                listRdv.Add(rdv);
            }
            return listRdv;
        }

        public Rdv update(Rdv obj)
        {
            throw new NotImplementedException();
        }


        //nouvelle méthode par rapport aux méthodes de base
        public List<Rdv> readAllByPerson(Personne personne)
        {
            if (personne is Patient || personne is Medecin)
            {
                List<Rdv> listRdv = new List<Rdv>();

                SqlCommand command = connection.CreateCommand();
                //Choisir si
                string condition = (personne is Medecin) ? "fk_idMedecin = " : "fk_idPatient = "; //condition ternaire: condition ? valeur si vrai : valeur si faux;
                //Deuxième partie de la condition, pour sélectionner l'id
                condition += (personne is Medecin) ? ((Medecin)personne).IdMedecin : ((Patient)personne).IdPatient;
                string query = "SELECT idRdv, date, fk_idMedecin, fk_idPatient FROM rdv WHERE " + condition;
                command.CommandText = query;

                //pour travailler en mode hors-connexion qui va stocker toutes nos requêtes dans un seul et même objet
                //mon objet nSqlDataAdapter a ma connexion à ma DB et a aussi la commande
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                //comme ici on n'a qu'une seule query, on appelle directement DataTable et pas DataSet
                DataTable dt = new DataTable();
                //on rempli notre DataTable avec la requête
                da.Fill(dt);

                //on récupère le médecin et le patient par rapport à la foreign key
                //Donc on utilise les DAO respectifs
                DAOMedecin daoMed = new DAOMedecin();
                DAOPatient daoPat = new DAOPatient();

                foreach (DataRow row in dt.Rows)
                {
                    //On crée des objets pour récupérer les id médecin ou patient
                    Medecin med;
                    Patient pat;
                    if (personne is Medecin)
                    {
                        //Si c'est un médecin, on connait déjà son id, donc on n'a pas besoin de le rechercher à nouveau
                        med = (Medecin)personne;
                        //grâce qau dao on retrouve l'id recherché
                        pat = daoPat.read((int)row["fk_idPatient"]);
                    }
                    else
                    {
                        //Si c'est un patient, on connait déjà son id, donc on n'a pas besoin de le rechercher à nouveau
                        med = daoMed.read((int)row["fk_idMedecin"]);
                        pat = (Patient)personne;
                    }
                    //on crée des rendez-vous avec les données récupérées
                    Rdv rdv = new Rdv((int)row["idRdv"], (DateTime)row["date"], med, pat);
                    //on crée une liste pour stocker les rendez-vous
                    listRdv.Add(rdv);
                }

                return listRdv;
            }
            //si ce n'est un médecin ni un patient (donc un secrétaire) on retourne null
            return null;
        }


    }
}
