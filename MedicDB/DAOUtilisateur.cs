﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAOUtilisateur : DAO, IDAO<Utilisateur>
    {
        public Personne Connection(string login, string password)
        {
            //voir si un utilisateur a ce login et ce password 
            //si personne ne l'a : return NULL
            //Sinon : on verifie si c'est medecin ou secretaire selon id
            // Puis renvoyer l'info


            SqlCommand command=connection.CreateCommand();
            //login=@login yra saugumo sumetimais. Login ir password galim nedeti, nes jie jau yra parmetruose
            string query = "SELECT idUtilisateur FROM utilisateur WHERE login= @login AND password= @password";
                
            command.CommandText = query;

            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);
          

            //int? - ziurim, ar int=null
            int? id = null;
            try
            {
               connection.Open();
               id = (int?)command.ExecuteScalar();
                
                //connection.Close(); galim deti cia arba pacioj pabaigoj, kai jo nededam i Finally
            }
            //mes galim sukurti savo nuosavas exceptions
            catch (Exception)
            {
                connection.Close();
                return null;
            }
            //finally n'est pas obligatoir
            finally
            {
                 connection.Close();
            }
            //connection.Close(); galim deti cia

            if (id == null) // cia tik if ir viskas, nera else
            {
                return null; // il va sortir de la methode
            }

            //gamima apsieiti ir be sito:  SqlCommand commandMedecin = connection.CreateCommand();
            command.CommandText = 
            "SELECT idMedecin FROM medecin " +                
            "WHERE fk_idUtilisateur= " + id;  //id, kurios iekojom ankstesneje paieskoje
           
            connection.Open();
            int? idMedecin = (int?)command.ExecuteScalar();
            connection.Close();
            if (idMedecin != null)  //id de medecin!
            {
                DAOMedecin daomed = new DAOMedecin();
                Medecin m = daomed.read((int)idMedecin);
                return m;
            }

            //jei tai ne daktaras, tai - sekretore:
            
            command.CommandText = "SELECT idSecretaire FROM secretaire " +
            "WHERE fk_idUtilisateur= " + id;  //id, kurios iekojom ankstesneje paieskoje
    
               connection.Open();
               int? idSecretaire = (int?)command.ExecuteScalar();
               connection.Close();

           if(idSecretaire != null) // jei tai sekretores id, ieskom ir afisuojam sekretores duomenis
            {
                DAOSecretaire daosec = new DAOSecretaire();
                Secretaire s = daosec.read((int)idSecretaire);
                return s;
                //arba: return daosec.read((int)idSecretaire
            }
            return null; //jei id ne sekretores    
           

        }
        public Utilisateur create(Utilisateur obj)
        {
            SqlCommand commandCreateUt = connection.CreateCommand();

            string queryCreateUt = "INSERT INTO utilisateur (login, password) " +
            "OUTPUT inserted.idUtilisateur " +
            "VALUES (@login, @password)";

            commandCreateUt.CommandText = queryCreateUt;

            commandCreateUt.Parameters.AddWithValue("@login", obj.Login);
            commandCreateUt.Parameters.AddWithValue("@password", obj.Password);

            connection.Open();
            int idUtilisateur = (int)commandCreateUt.ExecuteScalar();
            connection.Close();

            return obj;
        }

        internal Utilisateur GetFromMedecin(int id)
        {
            SqlCommand command = connection.CreateCommand(); //CreateCommand
            string query = @"SELECT  U.* from Utilisateur U
                inner join Medecin p on p.fk_idUtilisateur=u.idUtilisateur
                WHERE idMedecin = " + id;

            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Utilisateur medecin = null;
            foreach (DataRow row in dt.Rows)
            {
                medecin = new Utilisateur()
                {
                    IdUtilisateur = (int)row["idUtilisateur"],
                    Login = row["Login"].ToString()
                };

            }
            return medecin;
        }
        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Utilisateur read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM utilisateur WHERE idUtilisateur= " + id;

            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Utilisateur utilisateur = null;
            foreach (DataRow row in dt.Rows)
            {
                utilisateur = new Utilisateur(
                      (int)row["idUtilisateur"],
                      row["login"].ToString(),
                      row["password"].ToString());
            }

            return utilisateur;
        }

        public List<Utilisateur> readAll()
        {
            throw new NotImplementedException();
        }

        public Utilisateur update(Utilisateur obj)
        {
            throw new NotImplementedException();
        }

        public Utilisateur read(object id)
        {
            throw new NotImplementedException();
        }
    }
}
