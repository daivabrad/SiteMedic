﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Personne
    {
        private int id;
        private string nom;   
        private string prenom;
        private string adresse;
        private string numTelephone;
        

        public int Id { get => id; set => id = value; }
        [Required]
        public string Nom { get => nom; set => nom = value; }
        [DisplayName("Prénom")]
        public string Prenom { get => prenom; set => prenom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string NumTelephone { get => numTelephone; set => numTelephone = value; }

        public Personne()
        {

        }

        //cia du konstruktoriai
        public Personne (string nom, string prenom, string adresse, string numTelephone)
        {
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumTelephone = numTelephone;
        }
        public Personne(int id, string nom, string prenom, string adresse, string numTelephone)
            : this(nom, prenom, adresse, numTelephone)
        {
            Id = id;
        }
    }
}
