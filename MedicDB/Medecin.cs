﻿using System.Collections.Generic;

namespace MedicDB
{
    public class Medecin : Personne
    {
        private int idMedecin;
        private string nomSpecialisation;
        private int idSpecialisation;
        private string numINAMI;
        //private List<Rdv> listRdv;
        private Utilisateur _Utilisateur;
        public Utilisateur Utilisateur
        {
            get
            {
                if (_Utilisateur == null)
                {
                    DAOUtilisateur DU = new DAOUtilisateur();
                    _Utilisateur = DU.GetFromMedecin(this.Id);
                }
                return _Utilisateur;
            }
            set { _Utilisateur = value; }
        }

        public string NomSpecialisation { get => nomSpecialisation; set => nomSpecialisation = value; }
        public int IdMedecin { get => idMedecin; set => idMedecin = value; }
        public string NumINAMI { get => numINAMI; set => numINAMI = value; }
        //public List<Rdv> ListRdv { get => listRdv; set => listRdv = value; }
        public Utilisateur Utilisateur1 { get => Utilisateur; set => Utilisateur = value; }
        public int IdSpecialisation { get => idSpecialisation; set => idSpecialisation = value; }

        public Medecin(string nom, string prenom, string adresse, string numTelephone, string nomSpecialisation, string numINAMI)
            :base(nom, prenom, adresse, numTelephone)
        {
            NomSpecialisation = nomSpecialisation;
            NumINAMI= numINAMI;
        }

        public Medecin(int idMedecin, string nom, string prenom, string adresse, string numTelephone, string nomSpecialisation, string numINAMI)
            : this(nom, prenom, adresse, numTelephone, nomSpecialisation, numINAMI)
        {
            IdMedecin = idMedecin;
        }
        public Medecin()
        {

        }
    }
}