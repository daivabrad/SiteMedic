﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAOPatient : DAO, IDAO<Patient>
    {
        public Patient create(Patient obj)
        {
            SqlCommand commandCreatePerson = connection.CreateCommand();
            string queryCreatePerson = "INSERT INTO personne (nom, prenom, adresse, num_telephone) " +
               "OUTPUT inserted.idPersonne " +
               "VALUES (@nom, @prenom, @adresse, @num_telephone)";

            commandCreatePerson.CommandText = queryCreatePerson;

            commandCreatePerson.Parameters.AddWithValue("@nom", obj.Nom);
            commandCreatePerson.Parameters.AddWithValue("@prenom", obj.Prenom);
            commandCreatePerson.Parameters.AddWithValue("@adresse", obj.Adresse);
            commandCreatePerson.Parameters.AddWithValue("@num_telephone", obj.NumTelephone);
            
            connection.Open();
            int idPersonne = (int)commandCreatePerson.ExecuteScalar();
            connection.Close();

            SqlCommand commandCreatePatient = connection.CreateCommand();
            string queryCreatePatient = "INSERT INTO patient (DDN, sexe, registreNational, " +
                "medecinReferant, mutuelle, tierPayant, dateInscription, fk_idPersonne) " +
               "OUTPUT inserted.idPatient " +
               "VALUES (@DDN, @sexe, @registreNational, @medecinReferant, @mutuelle, @tierPayant, " +
               "@dateInscription, @idPersonne)";

            commandCreatePatient.CommandText = queryCreatePatient;

            commandCreatePatient.Parameters.AddWithValue("@DDN", obj.DDN);
            commandCreatePatient.Parameters.AddWithValue("@sexe", obj.Sexe);
            commandCreatePatient.Parameters.AddWithValue("@registreNational", obj.RegistreNational);
            commandCreatePatient.Parameters.AddWithValue("@medecinReferant", obj.MedecinReferant);
            commandCreatePatient.Parameters.AddWithValue("@mutuelle", obj.Mutuelle);
            commandCreatePatient.Parameters.AddWithValue("@tierPayant", obj.TierPayant);
            commandCreatePatient.Parameters.AddWithValue("@dateInscription", obj.DateInscription);
            commandCreatePatient.Parameters.AddWithValue("@idPersonne", idPersonne);

            connection.Open();
            int id = (int)commandCreatePatient.ExecuteScalar();
            connection.Close();

            return obj;
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Patient read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            //ici on utilise INNER JOIN, car on sait que la clé est NOT NULL (obligatoire) sinon, on aurait utilisé LESS JOIN
            string query = "SELECT nom, prenom, adresse, num_telephone, idPatient, " +
                "sexe, DDN, registreNational, medecinReferant, mutuelle, " +
                "tierPayant, dateInscription FROM patient pat INNER JOIN personne p ON pat.fk_idPersonne = p.idPersonne WHERE idPatient = " + id;
            command.CommandText = query;

            //pour travailler en mode hors-connexion qui va stocker toutes nos requêtes dans un seul et même objet
            //sQLdATAaDAPTER GèRE LES CONNEXIONs ET DéCONNEXIONs POUR NOUS
            SqlDataAdapter da = new SqlDataAdapter();
            //Il contient un objet SelectComment
            da.SelectCommand = command;
            //on peut avoir plusieurs DataTable dans DataSet: une table c'est UNE requête
            //Dans les DataTable on a des DataRow et des DataColumn
            //comme ici on n'a qu'une seule query, on appelle directement DataTable et pas DataSet
            DataTable dt = new DataTable();
            //on rempli notre DataTable avec la requête
            da.Fill(dt);

            //on va dire que par défaut, le patient est null, au cas où on entre un id inconnu (pour qu'il renvoie null (on fera un return patient)
            Patient patient = null;

            foreach (DataRow row in dt.Rows)
            {
                //on passe le paramètre en string et pas un int qui donne l'index de la valeur qu'on veut récupérer => exemple: "id¨Patient" au lieu de 5
                //on spécifie au reader qui'il lit des (int), et les paramètres qui doivent renvoyer des sting on leur joint .ToString()
                patient = new Patient(
                    (int)row["idPatient"],
                    row["nom"].ToString(),
                    row["prenom"].ToString(),
                    row["adresse"].ToString(),
                    row["num_telephone"].ToString(),
                    (DateTime)row["DDN"],
                    row["sexe"].ToString(),
                    row["registreNational"].ToString(),
                    row["medecinReferant"].ToString(),
                    row["mutuelle"].ToString(),
                    row["tierPayant"].ToString(),
                    (DateTime)row["dateInscription"]);
            }

            return patient;
        }

        public List<Patient> readAll()
        {
            List<Patient> patients = new List<Patient>();

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT nom, prenom, adresse, num_telephone, " +
                "idPatient, sexe, DDN, registreNational, medecinReferant, " +
                "mutuelle, tierPayant, dateInscription " +
                "FROM patient pat INNER JOIN personne p ON pat.fk_idPersonne = p.idPersonne";

            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter(command);

            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Patient patient = mapperPatient(row);
                patients.Add(patient);
            }
            return patients;
        }
        public Patient mapperPatient(DataRow row)
        {
            Patient patient = new Patient(
            (int)row["idPatient"],
            row["nom"].ToString(),
            row["prenom"].ToString(),
            row["adresse"].ToString(),
            row["num_telephone"].ToString(),
            (DateTime)row["ddn"],
            row["sexe"].ToString(),
            row["registreNational"].ToString(),
            row["medecinReferant"].ToString(),
            row["mutuelle"].ToString(),
            row["tierPayant"].ToString(),
            (DateTime)row["dateInscription"]);

            return patient;
        }

        public Patient readRegistreNational(string registreNational)
        {
            SqlCommand command = connection.CreateCommand();
            
            string query = "SELECT nom, prenom, adresse, num_telephone, idPatient, sexe, registreNational, DDN, " +
                "dateInscription FROM patient pat INNER JOIN personne p ON pat.fk_idPersonne = p.idPersonne WHERE registreNational = @registreNational";
            command.CommandText = query;

            command.Parameters.AddWithValue("@registreNational", registreNational);
           
            SqlDataAdapter da = new SqlDataAdapter();
            
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Patient patient = null;

            foreach (DataRow row in dt.Rows)
            {
                patient = new Patient(
                    (int)row["idPatient"], 
                    row["nom"].ToString(), 
                    row["prenom"].ToString(), 
                    row["adresse"].ToString(), 
                    row["num_telephone"].ToString(), 
                    (DateTime)row["DDN"], 
                    row["sexe"].ToString(), 
                    row["registreNational"].ToString(), (DateTime)row["dateInscription"]);
            }

            return patient;

        }


        public Patient read(object id)
        {
            throw new NotImplementedException();
        }

        public Patient update(Patient obj)
        {
            SqlCommand command = connection.CreateCommand();
            string queryGetIdPersonne = "SELECT fk_idPersonne FROM patient " +
                "pat INNER JOIN personne p ON pat.fk_idPersonne = p.idPersonne";
            command.CommandText = queryGetIdPersonne;
           
            connection.Open();
            int fk_idPersonne = (int)command.ExecuteScalar();
            connection.Close();

            SqlCommand commandUpdatePerson = connection.CreateCommand();

            string query1 = "UPDATE personne " +
                "SET Nom=@nom, Prenom=@prenom, Adresse=@adresse, Num_telephone=@num_telephone " +
                "WHERE idPersonne = " + fk_idPersonne;

            commandUpdatePerson.CommandText = query1;
            commandUpdatePerson.Parameters.AddWithValue("@nom", obj.Nom);
            commandUpdatePerson.Parameters.AddWithValue("@prenom", obj.Prenom);
            commandUpdatePerson.Parameters.AddWithValue("@adresse", obj.Adresse);
            commandUpdatePerson.Parameters.AddWithValue("@num_telephone", obj.NumTelephone);
            //commandUpdatePerson.Parameters.AddWithValue("id", obj.Id);

            connection.Open();
            commandUpdatePerson.ExecuteNonQuery();
            connection.Close();

            SqlCommand commandUpdatePatient = connection.CreateCommand();

            string query2 = "UPDATE patient " +
                "SET sexe=@sexe, registreNational=@registreNational, " + 
                "MedecinReferant=@medecinReferant, mutuelle=@mutuelle, tierPayant=@tierPayant " +
                "WHERE idPatient=@idPatient";

            commandUpdatePatient.CommandText = query2;

            commandUpdatePatient.Parameters.AddWithValue("@sexe", obj.Sexe);
            commandUpdatePatient.Parameters.AddWithValue("@registreNational", obj.RegistreNational);
            commandUpdatePatient.Parameters.AddWithValue("@medecinReferant", obj.MedecinReferant);
            commandUpdatePatient.Parameters.AddWithValue("@mutuelle", obj.Mutuelle);
            commandUpdatePatient.Parameters.AddWithValue("@tierPayant", obj.TierPayant);
            commandUpdatePatient.Parameters.AddWithValue("idPatient", obj.IdPatient);


            connection.Open();
            commandUpdatePatient.ExecuteNonQuery();
            connection.Close();

            Patient patient = null;
            return patient;

        }




        //public List<Patient> readAll()
        //{
        //    throw new NotImplementedException();
        //}


    }
}
