﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAO
    {
        //connection for the BD MedicDB!!!!

        private const string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=MedicDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //static reiskia, kad visos klases tures ta pati connection
        protected static SqlConnection connection
            =new SqlConnection(connectionString);
       
        public DAO()
        {

        }
        private SqlConnection GetSqlConnection()
        {
            return connection;
        }
       
    }
}
