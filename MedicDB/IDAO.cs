﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    //interface generique
    public interface IDAO<T>
    {
        //Type generique
        T create(T obj);
        T read(object id);
        List<T> readAll();
        T update(T obj);
        void delete(int id);
    }
}
