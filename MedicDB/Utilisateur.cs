﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class Utilisateur
    {
        private int idUtilisateur;
        private string login;
        private string password;
        private string color;

        public int IdUtilisateur { get => idUtilisateur; set => idUtilisateur = value; }
        public string Login { get => login; set => login = value; }
        public string Password { get => password; set => password = value; }
        public string Color { get => color; set => color = value; }

        public Utilisateur(int idUtilisateur, string login, string password)
        {
            IdUtilisateur = idUtilisateur;
            Login = login;
            Password = password;
        }

        public Utilisateur()
        {

        }

    }
}
