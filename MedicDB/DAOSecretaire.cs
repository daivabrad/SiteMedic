﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicDB
{
    public class DAOSecretaire : DAO, IDAO<Secretaire>
    {
        public Secretaire create(Secretaire obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }
        public Secretaire read(int id)
        {

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT nom, prenom, adresse, num_telephone, idSecretaire, service, u.idUtilisateur " +
                "FROM secretaire s " +
                "INNER JOIN personne p ON " +
                "s.fk_idPersonne = p.idPersonne " +
                "INNER JOIN utilisateur u ON " +
                "s.fk_idUtilisateur = u.idUtilisateur " +
                "WHERE idSecretaire= " + id;

            command.CommandText = query;


            SqlDataAdapter da = new SqlDataAdapter();
            
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            DAOUtilisateur daoU = new DAOUtilisateur();
            Secretaire secretaire = null;
            foreach (DataRow row in dt.Rows)
            {
                Utilisateur utilisateur;
                utilisateur = daoU.read((int)row["idUtilisateur"]);

                secretaire = new Secretaire(
                  (int)row["idSecretaire"],
                  row["nom"].ToString(),
                  row["prenom"].ToString(),
                  row["adresse"].ToString(),
                  row["num_telephone"].ToString(),
                  row["service"].ToString(), utilisateur
                  );
           
            }

            return secretaire;
        }

        public Secretaire read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Secretaire> readAll()
        {
            throw new NotImplementedException();
        }

        public Secretaire update(Secretaire obj)
        {
            throw new NotImplementedException();
        }
    }
}
