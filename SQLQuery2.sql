﻿CREATE TABLE personne (
idPersonne int PRIMARY KEY IDENTITY(1,1),
nom varchar(100) NOT NULL,
prenom varchar(100) NOT NULL,
adresse varchar(250) NOT NULL,
num_telephone varchar(20)
)

CREATE TABLE utilisateur (
idUtilisateur int PRIMARY KEY IDENTITY(1,1),
[login] varchar(50) NOT NULL,
[password] varchar(50) NOT NULL
)

CREATE TABLE specialisation (
idSpecialisation int PRIMARY KEY IDENTITY(1,1),
nom varchar(100) NOT NULL
)

CREATE TABLE secretaire (
idSecretaire int PRIMARY KEY IDENTITY(1,1),
[service] varchar(250) NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne),
fk_idUtilisateur int NOT NULL,

CONSTRAINT fk_secretaire_utilisateur FOREIGN KEY (fk_idUtilisateur) REFERENCES utilisateur (idUtilisateur)
)


CREATE TABLE medecin (
idMedecin int PRIMARY KEY IDENTITY(1,1),
numINAMI varchar(50) NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne),
fk_idUtilisateur int NOT NULL,
fk_idSpecialisation int NOT NULL REFERENCES specialisation (idSpecialisation),
CONSTRAINT fk_medecin_utilisateur FOREIGN KEY (fk_idUtilisateur) REFERENCES utilisateur (idUtilisateur)
)

CREATE TABLE patient (
idPatient int PRIMARY KEY IDENTITY(1,1),
DDN date NOT NULL,
sexe varchar(25) NOT NULL CHECK (sexe IN ('homme', 'femme', 'autre')),
registreNational varchar(25) UNIQUE NOT NULL,
medecinReferant varchar(75),
mutuelle varchar(75),
tierPayant varchar(100),
dateInscription datetime NOT NULL,
fk_idPersonne int NOT NULL REFERENCES personne (idPersonne)
)


CREATE TABLE dossierMedical (
idDossierMedical int PRIMARY KEY IDENTITY(1,1),
fk_idPatient int NOT NULL REFERENCES patient (idPatient)
)

CREATE TABLE consultation (
idConsultation int PRIMARY KEY IDENTITY(1,1),
nbMinutes int NOT NULL,
objet varchar(200) NOT NULL,
fk_idMedecin int NOT NULL REFERENCES medecin (idMedecin),
fk_idDossierMedical int NOT NULL REFERENCES dossierMedical (idDossierMedical)
)

CREATE TABLE pathologie (
idPathologie int PRIMARY KEY IDENTITY(1,1),
nom varchar(150) NOT NULL UNIQUE
)

CREATE TABLE consultation_pathologie (
idConsultationPathologie int PRIMARY KEY IDENTITY(1,1),
dateDebut date NOT NULL,
dateFin date,
contreIndication varchar(MAX),
fk_idConsultation int NOT NULL REFERENCES consultation (idConsultation),
fk_idPathologie int NOT NULL REFERENCES pathologie (idPathologie)
)

CREATE TABLE typeMedicament (
idTypeMedicament int PRIMARY KEY IDENTITY(1,1),
nom varchar(200) NOT NULL,
substance varchar(100) NOT NULL,
estGenerique bit NOT NULL
)

CREATE TABLE traitementMedicament (
idTraitementMedicament int PRIMARY KEY IDENTITY(1,1),
duree timestamp,
posologie varchar(200) NOT NULL,
fk_idTypeMedicament int NOT NULL REFERENCES typeMedicament (idTypeMedicament),
fk_idConsultation int NOT NULL REFERENCES consultation (idConsultation)
)

CREATE TABLE rdv (
idRdv int PRIMARY KEY IDENTITY(1,1),
[date] datetime NOT NULL,
fk_idMedecin int NOT NULL REFERENCES medecin (idMedecin),
fk_idPatient int NOT NULL REFERENCES patient (idPatient)
)

CREATE TABLE typeExamen (
idTypeExamen int PRIMARY KEY IDENTITY(1,1),
nom varchar(150) NOT NULL UNIQUE
)

/*DROP TABLE prescriptionExamen*/

CREATE TABLE prescriptionExamen (
idPrescriptionExamen int PRIMARY KEY IDENTITY(1,1),
remarques varchar(MAX),
localisiation varchar(75) NOT NULL,
fk_idConsultation int NOT NULL REFERENCES consultation (idConsultation),
fk_idTypeExamen int NOT NULL REFERENCES typeExamen(idTypeExamen)
)